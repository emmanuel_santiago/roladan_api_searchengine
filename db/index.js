'use strict';
const environment = require('../config/environment')
const { Client } = require('pg');
const debug = require('debug')('roldanSearchEngine');

exports._query = (query)=>{
    debug(`${environment.DB_HOST} ${environment.DB_NAME}`);
    return new Promise((resolve,rejected)=>{
        const client = new Client({
            host: environment.DB_HOST,
            port: environment.DB_PORT,
            user: environment.DB_USER,
            password: environment.DB_PASSWORD,
            database : environment.DB_NAME
        })
        client.connect();
        client.query(query,(err,resp)=>{
            client.end();
            if(err) rejected(err)
            else resolve(resp)
        })
    })
}