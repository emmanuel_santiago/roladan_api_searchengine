const log4js = require('log4js')
const db = require('../../db')

const logger = log4js.getLogger();
logger.level = 'debug';

exports.init = async (req,res,next) => {
    console.log(req.query)
    try {
        const q = req.query.q
        if(q){

            const query_fun = (view, cols, query) => {
                let Query = `SELECT * FROM "${view}" WHERE `
                cols.forEach((element, index) => {
                    if(index !== cols.length -1){
                        Query += `"${element}" ILIKE '%${query}%' OR `
                    }
                    if(index === cols.length -1){
                        Query += `"${element}" ILIKE '%${query}%' `
                    }
                });
                return Query
            }
            
            /**remover AduanaId */
            const Impo_columns = ['DOAduanaImpo', 'DocTransporte', 'RefCliente', 'Aduana', 'ModoTransporte', 'TransportadorInternacional', 'PuertoOrigen', 'PuertoDestino', 'Estatus', 'Cliente']
            const Importaciones = await db._query(query_fun('vwImportaciones', Impo_columns, q))
            
            const Expo_columns = ['DOAduanaExpo', 'DocTransporte', 'ModoTransporte', 'RefCliente', 'Aduana', 'Proyecto', 'TransportadorInt', 'Consignatario', 'AgenteCarga', 'Cliente']
            const Exportaciones = await db._query(query_fun('vwExportaciones', Expo_columns, q))
    
            const Inter_columns = ['DoEmbarque', 'Proveedor', 'Consignatario', 'Delivery', 'RefCliente', 'HBL_HAWB', 'MBL_MAWB', 'Carrier', 'AgenteAduanal', 'CoLoader', 'NoMotonaveVuelo', 'POL', 'OrigenNombre', 'POD', 'DestinoNombre', 'CodIncoterm', 'Incoterm', 'Proyecto', 'IMO', 'ModoTransporte', 'DestinoFinal', 'TipoNegocio', 'Naturaleza', 'Estatus', 'Cliente', 'RecoleccionEntrega' ]
            const Internacional = await db._query(query_fun('vwEmbarques', Inter_columns, q))
    
            const Des_columns = ['Almacen', 'RefCliente', 'DocTransporte', 'PlacaVehiculo', 'Destinatario', 'Cliente']
            const Despachos = await db._query(query_fun('vwDespachos', Des_columns, q))

            const Ingr_columns = ['Almacen', 'TipoAlmacen', 'DoAduana', 'RefCliente', 'DocTransporte', 'Documento', 'TipoDocumento', 'Proceso', 'TipoDocumentoEspecifico', 'TipoVehiculo', 'Estatus', 'Cliente']
            const Ingresos = await db._query(query_fun('vwIngresos',Ingr_columns, q))

            //console.log("=====>",query_fun('vwEmbarques', Inter_columns, q))
    
            const response = {
                Importaciones: {
                    Importaciones: Importaciones.rows,
                    Total: Importaciones.rowCount
                },
                Exportaciones: {
                    Exportaciones: Exportaciones.rows,
                    Total: Exportaciones.rowCount
                },
                Internacional: {
                    Internacional: Internacional.rows,
                    Total: Internacional.rowCount
                },
                Ingresos: {
                    Ingresos: Ingresos.rows,
                    Total: Ingresos.rowCount
                },
                Despachos:{
                    Despachos: Despachos.rows,
                    Total: Despachos.rowCount
                },
                Total: Importaciones.rowCount + Exportaciones.rowCount + Internacional.rowCount + Despachos.rowCount + Ingresos.rowCount
            }
            
            let msg 
            response.Total ? msg = response : msg = 'No se encontraron coincidencias'

            res.status(200).send({
                Input: q,
                message: msg
            })

        }else{
            res.status(200).send({
                Input: q,
                message: 'No se encontraron coincidencias'
            })
        }
    } catch (error) {
        console.log(error)
        res.status(500).send({
            status: 'ERROR',
            message: "DATA BASE ERROR"
        })
    }
}