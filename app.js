'use strict';
const express = require('express');
const logger = require('morgan');
const app = express();
/**
 * Import Routes
 */
const indexRoutes = require('./routes');
const quickSearch = require('./routes/quickSearch')

/**
 * Using Routes
 */
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/', indexRoutes);
app.use('/quicksearch', quickSearch)

module.exports = app;