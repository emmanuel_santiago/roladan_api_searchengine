'use strict';
const app = require('./app');
const debug = require('debug')('users');
const log4js = require('log4js')
const http = require('http');

var logger = log4js.getLogger();
logger.level = 'debug';

/**
 * Get port from environment and store in Express.
 */
let port = process.env.PORT || '3020';
app.set('port', port);
/**
 * Create HTTP server.
 */
const server = http.createServer(app);
/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('listening', onListening);
/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  logger.debug('Listening on port:',port);
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
};
