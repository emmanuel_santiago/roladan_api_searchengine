'use strict';
const api = require('express').Router();
const log4js = require('log4js')


const Aduana = require('../../controllers/exportaciones')

var logger = log4js.getLogger();
logger.level = 'debug';

module.exports = (()=>{
    /**
     * Receive and validate input data
     */
    api.post('/',
        Schema.validateExportaciones,
        Aduana.saveExportaciones,
    )

    api.put('/',
        Schema.validateExportaciones,
        Aduana.updateAndSaveExports,
    )

    return api;
})();