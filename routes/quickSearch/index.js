'use strict';
const api = require('express').Router();
const log4js = require('log4js')

const QuickSearch = require('../../controllers/quickSearch')

const logger = log4js.getLogger();
logger.level = 'debug';

module.exports = (()=>{
    /**
     * Receive and validate input data
     */
    api.get('/',
        QuickSearch.init
    )

    return api;
})();